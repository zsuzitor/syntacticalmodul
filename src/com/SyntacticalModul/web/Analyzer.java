package com.SyntacticalModul.web;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.linalg.Vector;

import com.SyntacticalModul.work.Comparator;
import com.SyntacticalModul.work.LDA;
import com.SyntacticalModul.work.PostgreDataBase;
import com.SyntacticalModul.work.Tokenizer;




/**
 * Servlet implementation class Analyzer
 */
@WebServlet("/Analyzer")
public class Analyzer extends HttpServlet  implements Serializable{
	
	transient JavaSparkContext sc;
	LDA lda;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Analyzer() {
        SparkConf conf = new SparkConf().setAppName("LDA Analyzer").setMaster("local[2]").set("spark.executor.memory", "1g");
	    sc = new JavaSparkContext(conf);
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		long time = 0;//����� ������
		double fulness= 0.0, accuracy = 0.0;//������� � �������� ������
		
		try{
			//�������� ��������� ������
			String _abstract = request.getParameter("abstract");
			String description = request.getParameter("description");
			String claim = request.getParameter("claim");	
			String expectedResult = request.getParameter("expectedresult");	
			double delta = Double.parseDouble(request.getParameter("delta"));	
			
			String tokenizertype = request.getParameter("tokenizer");
			String storagetype = request.getParameter("storage");
			String filtertype = request.getParameter("filter");
			
			String pathToCorpus = request.getParameter("pathtocorpus");
			String pathToSynonyms = request.getParameter("pathtosynonyms");
			String pathToStopWords = request.getParameter("pathtostopwords");
			
			
			String host = request.getParameter("host");
			String port = request.getParameter("port");
			String dbname = request.getParameter("dbname");
			String user = request.getParameter("user");
			String password = request.getParameter("password");
			
			List <String> expectedIDs = Arrays.asList(expectedResult.split(","));
			long lBegin = System.currentTimeMillis();
			
			
			Tokenizer tokenizer = new Tokenizer( pathToSynonyms, pathToStopWords);
			List <String> resultIDs = null;
			Vector filter = null;
			String patentText = "";
			int typeTable = 0;
			String functionName = "";
			
			//�� ������ ������ ���������� ������ ����������� ������������ ����� ��������� ������
			if(tokenizertype.equalsIgnoreCase("����������� � ��������� ����-����")){
				typeTable = 1;
				patentText = "0.txt\t" + tokenizer.tokenize(_abstract + " " + description + " " + claim, false);
				lda= new LDA(sc, pathToCorpus + "/withRemoveStopWords");
			}
			else if(tokenizertype.equalsIgnoreCase( "����������� � ������� ���������")){
				typeTable = 2;
				patentText = "0.txt\t" + tokenizer.tokenize(_abstract + " " + description + " " + claim, true);
				lda= new LDA(sc, pathToCorpus + "/withReplaceSynonyms");
			}
			else if(tokenizertype .equalsIgnoreCase( "����������� �� ������ n-�����")){
				typeTable = 3;
				patentText = "0.txt\t" + tokenizer.tokenizeWithNGramm(_abstract + " " + description + " " + claim);
				lda= new LDA(sc, pathToCorpus + "/withNGrams");
			}
			else {
				doSetError(response);
				return;
			}
			
			//�������� ������ ������������� ��������� �� �������
			ArrayList<String> arr = new ArrayList<String>();
			arr.add(patentText);
			JavaRDD<String> input = sc.parallelize(arr);
			JavaPairRDD<java.lang.Long,Vector> countVector = lda.getTermCountVectors(sc, input, false);			
			JavaPairRDD<Long,Vector> topicDistributions = lda.topicDistributions(countVector);
			filter = topicDistributions.collectAsMap().get(0);
			
			
			//�������� id ���������� ��������������� ������
			if(storagetype.equalsIgnoreCase("PostgreSQL")){
				PostgreDataBase db = new PostgreDataBase(host, port, dbname, user, password);
				if(filtertype.equalsIgnoreCase("������� ���� ����� ���������")){
					functionName = "compare_vector_by_cos";
					}
				else if(filtertype.equalsIgnoreCase("������������������ ����������")) {
					functionName = "compare_vector_by_rms";
					}
				else if(filtertype.equalsIgnoreCase("��������� ���� ��������")){
					functionName = "compare_vector_by_lenght";
				}
				else if(filtertype.equalsIgnoreCase( "��������� ������� �������� ������� ����� �����")){
					functionName = "compare_vector_by_difference";
					
				}
				else {
					doSetError(response);
					return;
					}
				resultIDs = Comparator.getFilteredVectorsByPostgreSQL(db, functionName, typeTable, filter, delta);
			}
			
			else if(storagetype.equalsIgnoreCase("HDFS")){
				System.out.println("isgood1");
				if(filtertype.equalsIgnoreCase("������� ���� ����� ���������")){
					resultIDs = Comparator.getFilteredVectorsByCos(lda.topicDistributions(), filter, delta);
					}
				else if(filtertype.equalsIgnoreCase("������������������ ����������")) {
					resultIDs = Comparator.getFilteredVectorsByRMS(lda.topicDistributions(), filter, delta);
					}
				else if(filtertype.equalsIgnoreCase("��������� ���� ��������")){
					resultIDs = Comparator.getFilteredVectorsByLenght(lda.topicDistributions(), filter, delta);
				}
				else if(filtertype.equalsIgnoreCase( "��������� ������� �������� ������� ����� �����")){
					resultIDs = Comparator.getFilteredVectorsByDifference(lda.topicDistributions(), filter, delta);
					
				}
				else {
					doSetError(response);
					return;
				}
			}
			else {
				doSetError(response);
				return;
			}
			
		
			//����� �������
			long lEnd = System.currentTimeMillis();
			time = (lEnd - lBegin)/1000;
			
			//������� ��������� ����������
			double countOfExpectedFinds = 0;
			for(int i = 0; i < resultIDs.size(); i++)
				if(expectedIDs.contains(resultIDs.get(i))) countOfExpectedFinds++;
			
			//������ ������� � �������� ������
			fulness = countOfExpectedFinds / expectedIDs.size();
			accuracy = countOfExpectedFinds /resultIDs.size();
			
			//����������� ����������
			String stringResult = "����� ������: " + time + " �; �������� ������: " + accuracy + "; ������� ������: " + fulness + "; ��������� ���������:";
			for(String s : resultIDs)
			{
				stringResult += " " + s;
			}
			doSetResult(response, stringResult);
			
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			doSetError(response);
		}
	}
	/**
	 * ������� ���������� ��������� ������������
	 * 
	 * @param response - ������
	 * @param result - ���������
	 * 
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	protected void doSetResult( HttpServletResponse response, String result ) throws UnsupportedEncodingException, IOException {
		String reply = "{\"error\":0,\"result\":\"" + result + "\"}";
		System.out.println(reply);
		response.getOutputStream().write( reply.getBytes("UTF-8") );
		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setStatus( HttpServletResponse.SC_OK );
	}		
	
	/**
	 * ������� ���������� ������ ������
	 * 
	 * @param response - ������
	 * 
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	protected void doSetError( HttpServletResponse response ) throws UnsupportedEncodingException, IOException {
		String reply = "{\"error\":1}";
		response.getOutputStream().write( reply.getBytes("UTF-8") );
		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setStatus( HttpServletResponse.SC_OK );
	}
}
