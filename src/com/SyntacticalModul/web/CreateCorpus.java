package com. SyntacticalModul.web;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;

import com.SyntacticalModul.work.LDA;
import com.SyntacticalModul.work.PostgreDataBase;
import com.SyntacticalModul.work.Tokenizer;


/**
 * Servlet implementation class CreateCorpus
 */
@WebServlet("/CreateCorpus")
public class CreateCorpus extends HttpServlet implements Serializable{
	transient JavaSparkContext sc;
	LDA lda;
	Tokenizer tokenizer;
    /**
     * @throws IOException 
     * @see HttpServlet#HttpServlet()
     */
    public CreateCorpus() throws IOException {
    	
    	
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//инициализируем Spark
        SparkConf conf = new SparkConf().setAppName("LDA Analyzer").setMaster("local[2]");
        sc = new JavaSparkContext(conf);
		try
		{
			//получаем входные параметры кластеризации
			String pathToPatents = request.getParameter("pathtopatents");
			String pathToSave = request.getParameter("pathtosave");
			String pathToSynonyms = request.getParameter("pathtosynonyms");
			String pathToStopWords = request.getParameter("pathtostopwords");
			
			String host = request.getParameter("host");
			String port = request.getParameter("port");
			String dbname = request.getParameter("dbname");
			String user = request.getParameter("user");
			String password = request.getParameter("password");
		
			int k = Integer.parseInt(request.getParameter("k"));
			int iterations = Integer.parseInt(request.getParameter("iterations"));
			
			
			
			//JavaRDD<String> documents = sc.textFile(pathToPatents);
		    tokenizer = new Tokenizer(pathToSynonyms,pathToStopWords);
		    /*//получаем документы токенизированные удалением стоп-слов
		   JavaRDD<String> documentsWithRemoveStopWords = documents.map(new Function<String, String>() {
				@Override
	            public String call(String document) throws Exception {
	                int idxFirstSpace = document.indexOf("\t");
	                
	                String id = document.substring(0, idxFirstSpace);
	                String words = tokenizer.tokenize(document.substring(idxFirstSpace+1), false);
	                
	                return id + "\t" + words;
	            }
	        });
		   
		  //получаем документы токенизированные с заменой синонимов
		    JavaRDD<String> documentsWithReplaceSynonyms = documents.map(new Function<String, String>() {
				@Override
	            public String call(String document) throws Exception {
	                int idxFirstSpace = document.indexOf("\t");
	                
	                String id = document.substring(0, idxFirstSpace);
	                String words = tokenizer.tokenize(document.substring(idxFirstSpace+1), true);
	               
	                return id + "\t" + words;
	            }
	        });
		    
		  //получаем документы токенизированные на основе ngram
		    JavaRDD<String> documentsWithNGrams = documents.map(new Function<String, String>() {
				@Override
	            public String call(String document) throws Exception {
	                int idxFirstSpace = document.indexOf("\t");
	                
	                String id = document.substring(0, idxFirstSpace);
	                String words = tokenizer.tokenizeWithNGramm(document.substring(idxFirstSpace+1));
	               
	                return id + "\t" + words;
	            }
	        });
		    */
			PostgreDataBase db = new PostgreDataBase(host,port,dbname, user,password);
			/*
			//кластеризируем на основе токенизации с удалением стоп-слов
		    lda = new LDA();
		    JavaPairRDD<java.lang.Long,Vector> docVectorsWithRemoveStopWords = lda.getTermCountVectors(sc, documentsWithRemoveStopWords, true);
		    lda.getLDAModel(docVectorsWithRemoveStopWords, k, iterations);
		    lda.save(sc, pathToSave + "/withRemoveStopWords");
		    db.insertIntoTableVectors("values_​​of_the_vectors_stop_words", lda.topicDistributions());
			
		  //кластеризируем на основе токенизации с заменой синонимов
		    lda = new LDA();
		    JavaPairRDD<java.lang.Long,Vector> docVectorsWithReplaceSynonyms = lda.getTermCountVectors(sc, documentsWithReplaceSynonyms, true);
		    lda.getLDAModel(docVectorsWithReplaceSynonyms, k, iterations);
		    lda.save(sc, pathToSave + "/withReplaceSynonyms");
		    db.insertIntoTableVectors("values_​​of_the_vectors_synonyms", lda.topicDistributions());
			*/
		  //кластеризируем на основе токенизации с заменой синонимов
			JavaRDD<String> documentsWithNGrams = sc.textFile("D:/120InputDataN");
		    lda = new LDA();
		    JavaPairRDD<java.lang.Long,Vector> docVectorsWithNGrams = lda.getTermCountVectors(sc, documentsWithNGrams, true);
		    lda.getLDAModel(docVectorsWithNGrams, k, iterations);
		    lda.save(sc, pathToSave + "/withNGrams");
		    db.insertIntoTableVectors("values_​​of_the_vectors_n_gram", lda.topicDistributions());
		    
		    
		    doSetResult(response,0);
		    
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
				 doSetResult(response,1);
		}
		finally
		{
			sc.stop();
		}
		   
	  
	}
	
	/**
	 * Функция возвращает результат кластеризации
	 * 
	 * @param response - запрос
	 * @param error - наличие ошибки
	 * 
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	protected void doSetResult( HttpServletResponse response, int error ) throws UnsupportedEncodingException, IOException {
		String reply = "{\"error\":" + error + "}";
		response.getOutputStream().write( reply.getBytes("UTF-8") );
		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setStatus( HttpServletResponse.SC_OK );
	}		
	
	

}
