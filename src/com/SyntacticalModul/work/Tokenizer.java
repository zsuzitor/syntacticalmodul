package com.SyntacticalModul.work;



import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.jasper.tagplugins.jstl.core.Out;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.ngram.NGramTokenFilter;
import org.apache.lucene.analysis.ngram.NGramTokenizer;
import org.apache.lucene.analysis.shingle.ShingleFilter;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.morphology.LuceneMorphology;
import org.apache.lucene.morphology.english.EnglishLuceneMorphology;
import org.apache.lucene.morphology.russian.RussianLuceneMorphology;
//
import org.apache.lucene.util.AttributeFactory;
import org.apache.lucene.util.AttributeSource;

/**
 * ����� ��� ����������� ������
 */
public class Tokenizer implements Serializable {

	private String PathToDictionarySynonyms;//���� � ������� ���������
	private String PathToVocabularyStopWords;//���� � ������� ����-����
	private List <List<String>> Synonyms;//������ ���������, � ������ ������� - �����,�������,�������,���.
	
	
	/**
	 * �����������, ������� ���������� �� ������ ����� � ��������
	 * 
	 * @param String pathToDictionarySynonyms - ���� � ������� ���������
	 * @param String pathToVocabularyStopWords - ���� � ������� ���� ����
	 * 
	 * @throws IOException
	 */
	public Tokenizer(String pathToDictionarySynonyms, String pathToVocabularyStopWords) throws IOException
	{
		PathToDictionarySynonyms = pathToDictionarySynonyms;
		PathToVocabularyStopWords = pathToVocabularyStopWords;
		
		//��������� ������ ���������
		Synonyms = new ArrayList<List<String>>();
		if(pathToDictionarySynonyms != null)
		{
			
			   BufferedReader in = new BufferedReader(new FileReader(PathToDictionarySynonyms));
		        String str = "";
		        while ((str = in.readLine()) != null)
		        {
		        	Synonyms.add(Arrays.asList(str.split(",")));
		        }
		        in.close();
		}
		
	}
	
	
	
	/** 
	 * ������� ����������� ������ � ��������� ����-����.
	 * 
	 * @param String text - ����� ��� ����������� (� ����� �������).
	 * @param boolean replaceSynonyms - ����������� � ������� ��������� ���� �������� TRUE
	 * @return String result - ������ � ���������������� ������� ��� ����-���� (������ ����������� �������� ',').
	 * 
	 * @throws IOException 
	 */
	public String tokenize(String text, boolean replaceSynonyms) throws IOException
	{
		
		String result = "";
		//������������� StopAnalyzer �� Lucene
		StopAnalyzer analyzer = new StopAnalyzer(FileSystems.getDefault().getPath( PathToVocabularyStopWords));  
        TokenStream stream = analyzer.tokenStream("contents", new StringReader(text));  
        
        //������ Morphology �� Lucene ��� ������������
        LuceneMorphology luceneMorphRU = new RussianLuceneMorphology(); 
        LuceneMorphology luceneMorphEN = new EnglishLuceneMorphology(); 
        stream.reset();
        while (stream.incrementToken()) {  
        	//��������� ������
        	AttributeSource token = stream.cloneAttributes();  
        	//��������� �����
            CharTermAttribute term =(CharTermAttribute) token.addAttribute(CharTermAttribute.class);  
             
            //������������ �����
            String wordInfo = "";
            
            try{
            	//������������ �������� �����
            	wordInfo= luceneMorphRU.getNormalForms(term.toString()).get(0);
            
            }
            catch( Exception e ){
            	if(luceneMorphEN.checkString(term.toString()))
            	//����� ������������� ����������� �����
            	wordInfo = luceneMorphEN.getNormalForms(term.toString()).get(0);  
            }
            finally {
            	
            	//���������� ����� � �������� ������
            	
            	 if(!wordInfo.equals("null") && wordInfo != null)
            	 {
            		 if(replaceSynonyms) result += getSynonym(wordInfo) + ",";
            		 else result += wordInfo + ","; 
            	 }
            	
            		 
            }   
        }  
        System.out.println(result);
        stream.end();
        stream.close();
         
		return result;
	}
	
	
	/** 
	 * ������� ��������� �������� �����.
	 * 
	 * @param String word - �����, ������� �������� ����.
	 * 
	 * @return  String result - ������� �����. ���������� ���� ����� ���� ������� �� ������.
	 */
	private String getSynonym(String word)
	{
		String result = "";
		
		//����� ������� �������� ���� �� ����� ������ �������
		for(int i = 0; i < Synonyms.size() && result.isEmpty(); i++)
		{
			//��������� ������ ��������� ��� ������ ������� ���������
			List<String> synsWord = Synonyms.get(i);
			
			//����� ��������� � ��������� �����
			for(String syn : synsWord)
			{
				//���� ������� ������
				if(syn.equals(word))
					result = synsWord.get(0);
			}
		}
		if(result.isEmpty()) result = word;
		return result;
	}
	/** 
	 * ������� ����������� ������ �� n-������.
	 * 
	 * @param String text - ����� ��� ����������� (� ����� �������).
	 *
	 * @return String result - ������ � ���������������� ������� �� n-������ (������ ����������� �������� ',').
	 * @throws IOException 
	 */
	public String tokenizeWithNGramm(String text) throws IOException
	{
		String result = "";
		
		SimpleAnalyzer analyzer = new SimpleAnalyzer();  
		
        TokenStream stream = analyzer.tokenStream("contents", new StringReader(tokenize(text, false)));  
        ShingleFilter ngramfilter = new ShingleFilter(stream, 3);
        ngramfilter.reset();
        while (ngramfilter.incrementToken()) {   
            AttributeSource token = ngramfilter.cloneAttributes();  
            CharTermAttribute term =(CharTermAttribute) token.addAttribute(CharTermAttribute.class);  
            
            String wordInfo = "";
            try{
           	 
           	 wordInfo= term.toString();
           }
           catch( Exception e ){
           	wordInfo = term.toString();  
           }
            finally {
           		 result += wordInfo + ",";
           	
			}
              
        }  
        ngramfilter.end();
        ngramfilter.close();
        return result;
	}
	
}

