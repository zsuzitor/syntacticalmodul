package com.SyntacticalModul.work;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.linalg.Vector;

import scala.Tuple2;

/**
 * ����� ����������� ������ ��������� ��������
 */
public abstract class Comparator {
	
	/**
	 * ������� ��������� id �������� ��������������� ������.
	 * 
	 * @param PostgreDataBase db - ���� ������
	 * @param String functionName - ��� ������� (������ ��������� ��������)
	 * @param int typeTable - ��� ������� (1,2 ��� 3)
	 * @param Vector filter - ������� ������ ��� ������, � ��� ������������ ������� �� ����
	 * @param double delta - �������� ������
	 * 
	 * @return List<String> - ������ id ��������
	 * 
	 * @throws SQLException
	 */
	public static List<String> getFilteredVectorsByPostgreSQL(PostgreDataBase db,String functionName,int typeTable, Vector filter, double delta) throws SQLException
	{
		ResultSet rs = db.callFunction(functionName, filter, typeTable, delta);
		String resString = "";
		rs.next();
		try
		{
			resString = rs.getString(1).replace('{', ' ').replace('}', ' ').trim();
		}
		catch( Exception ex)
		{
			resString = "";
		}
		return Arrays.asList(resString.split(","));
		
	}
	
	/**
	 * ������� ��������� id �������� ��������������� ������ ������� ������������������� ����������
	 * @param JavaPairRDD<Long, Vector> arrayOfVectors - ������ ��������
	 * @param Vector filter - ������� ������ ��� ������, � ��� ������������ ������� �� ����
	 * @param double delta - �������� ������
	 * 
	 * @return List<String> - ������ id ��������
	 */
	public static List<String> getFilteredVectorsByRMS(JavaPairRDD<Long, Vector> arrayOfVectors, final Vector filter, final double delta)
	{
		//���������� ������� ������ �������� ������� ��������
		JavaPairRDD<Long, Vector> filteredVectors = arrayOfVectors.filter(new Function<Tuple2<Long, Vector>, Boolean>() {
			
			public Boolean call(Tuple2<Long, Vector> keyValue) {
		    	Boolean isEqual = true;
		    	double result = 0;
		    	
		    	for(int i = 0; i < keyValue._2.size() && isEqual == true; i++)
		    	{
		    		result += (filter.apply(i) - keyValue._2.apply(i))*(filter.apply(i) - keyValue._2.apply(i));
		    		if(Math.sqrt(result/(i+1)) > delta) isEqual = false;
		    	}
		    	
		      return isEqual;
		    }
		  });
		  return getListOfIDs(filteredVectors);
	}
	
	/**
	 * ������� ��������� id �������� ��������������� ������ ������� ������������� ��������� ��������
	 * @param JavaPairRDD<Long, Vector> arrayOfVectors - ������ ��������
	 * @param Vector filter - ������� ������ ��� ������, � ��� ������������ ������� �� ����
	 * @param double delta - �������� ������
	 * 
	 * @return List<String> - ������ id ��������
	 */
	public static List<String> getFilteredVectorsByDifference(JavaPairRDD<Long, Vector> arrayOfVectors, final Vector filter, final double delta)
	{
		//���������� ������� ������ �������� ������� ��������
		JavaPairRDD<Long, Vector> filteredVectors = arrayOfVectors.filter(new Function<Tuple2<Long, Vector>, Boolean>() {
			
			public Boolean call(Tuple2<Long, Vector> keyValue) {
		    	Boolean isEqual = true;
		    	for(int i = 0; i < keyValue._2.size() && isEqual == true; i++)
		    	{
		    		if(Math.abs(filter.apply(i) - keyValue._2.apply(i)) > delta) isEqual = false;
		    	}
		    	
		      return isEqual;
		    }
		  });
		return getListOfIDs(filteredVectors);
	}
	
	/**
	 * ������� ��������� id �������� ��������������� ������ ������� ���������
	 * @param JavaPairRDD<Long, Vector> arrayOfVectors - ������ ��������
	 * @param Vector filter - ������� ������ ��� ������, � ��� ������������ ������� �� ����
	 * @param double delta - �������� ������
	 * 
	 * @return List<String> - ������ id ��������
	 */
	public static List<String>getFilteredVectorsByCos(JavaPairRDD<Long, Vector> arrayOfVectors, final Vector filter, final double delta)
	{
		//���������� ������� ������ �������� ������� ��������
		JavaPairRDD<Long, Vector> filteredVectors =arrayOfVectors.filter(new Function<Tuple2<Long, Vector>, Boolean>() {
			
			public Boolean call(Tuple2<Long, Vector> keyValue) {
		    	Boolean isEqual = true;
		    	double cos = 0;
		    	double partOfTheDenominatorFilter = 0;
		    	double partOfTheDenominatorI = 0;
		    	for(int i = 0; i < keyValue._2.size() && isEqual == true; i++)
		    	{
		    		cos += filter.apply(i)*keyValue._2.apply(i);
		    	     partOfTheDenominatorFilter += Math.pow(filter.apply(i), 2);
		    	     partOfTheDenominatorI += Math.pow(keyValue._2.apply(i), 2);
		    	}
		    	cos /= Math.sqrt(partOfTheDenominatorFilter);
		        cos /= Math.sqrt(partOfTheDenominatorI);
		        
		        if (1 - cos > delta) isEqual = false;
		      return isEqual;
		    }
		  });
		return getListOfIDs(filteredVectors);
	}
	
	/**
	 * ������� ��������� id �������� ��������������� ������ ������� ��������� ���� ��������
	 * @param JavaPairRDD<Long, Vector> arrayOfVectors - ������ ��������
	 * @param Vector filter - ������� ������ ��� ������, � ��� ������������ ������� �� ����
	 * @param double delta - �������� ������
	 * 
	 * @return List<String> - ������ id ��������
	 */
	public static List<String> getFilteredVectorsByLenght(JavaPairRDD<Long, Vector> arrayOfVectors, final Vector filter, final double delta)
	{
		//���������� ������� ������ �������� ������� ��������
		JavaPairRDD<Long, Vector> filteredVectors = arrayOfVectors.filter(new Function<Tuple2<Long, Vector>, Boolean>() {
			
			public Boolean call(Tuple2<Long, Vector> keyValue) {
				double lenghtOfFilter = 0;
				
				for(int i = 0; i < filter.size(); i++)
				{
					lenghtOfFilter += Math.pow(filter.apply(i),2);
				}
				lenghtOfFilter = Math.sqrt(lenghtOfFilter);
		    	Boolean isEqual = true;
		    	double lenghtOfVector = 0;
		    	for(int i = 0; i < keyValue._2.size() && isEqual == true; i++)
		    	{
		    		lenghtOfVector += Math.pow(keyValue._2.apply(i),2);
		    		
		    	}
		    	if(Math.abs(lenghtOfFilter - Math.sqrt(lenghtOfVector)) > delta) isEqual = false;
		      return isEqual;
		    }
		  });
		return getListOfIDs(filteredVectors);
	}
	
	/**
	 * ������� ��������� ������ id �� JavaPairRDD<Long, Vector>.
	 * 
	 * @param JavaPairRDD<Long, Vector> arrayOfVectors - id � ������� ����������
	 * @return
	 */
	private static List<String> getListOfIDs(JavaPairRDD<Long, Vector> arrayOfVectors)
	{
		List <String> result = new ArrayList<String>();
		Map <Long, Vector> mapOfVectors = arrayOfVectors.collectAsMap();
		
		for (Map.Entry<java.lang.Long,Vector> entry : mapOfVectors.entrySet())
		{
			result.add(entry.getKey().toString());
		}
	    
		return result;
	}
}
