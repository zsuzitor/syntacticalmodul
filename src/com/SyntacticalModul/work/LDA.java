package com.SyntacticalModul.work;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapred.SequenceFileOutputFormat;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.ml.feature.CountVectorizer;
import org.apache.spark.ml.feature.CountVectorizerModel;
import org.apache.spark.mllib.clustering.DistributedLDAModel;
import org.apache.spark.mllib.clustering.LDAModel;
import org.apache.spark.mllib.clustering.LocalLDAModel;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;



import org.apache.spark.sql.DataFrame;



import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.types.ArrayType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import scala.Tuple2;

/**
 * 
 * ����� ������������ ��� ������������� � �������� ������ LDA
 *
 */
public class LDA implements Serializable{
	
	private LocalLDAModel LLDAModel; // ��������� ������ LDA
	private transient DistributedLDAModel DLDAModel; // �������������� ������ LDA
	private CountVectorizerModel CVM; //������ ������������
	private JavaPairRDD<java.lang.Long,Vector> TopicDistributions;//������� ������������� ���������������� ����������
	
	/** 
	 * ����������� �� ���������.
	 * 
	 */
	public LDA()
	{
		LLDAModel = null;
	    DLDAModel = null;
		CVM = null;
	}
	/** 
	 * ����������� �������� ������ �� ������� ������ �� �����.
	 * 
	 * @param SparkContext sc - ��������.
	 * @param java.lang.String path - ���� �������� ������.
	 */
	public LDA(JavaSparkContext sc, java.lang.String path)
	{
		load(sc ,path);
	}
	
	/** 
	 * ������� ��������� ������� ������������� ��� documents �� ��������� LDAModel.
	 * 
	 * @param JavaPairRDD<java.lang.Long,Vector> documents - ��������� ��� ��������� �������������.
	 * 
	 * @return JavaPairRDD<java.lang.Long,Vector> - ������� ������������� �� ������� ������� ����������.
	 */
	public JavaPairRDD<java.lang.Long,Vector> topicDistributions(JavaPairRDD<java.lang.Long,Vector> documents)
	{
		return LLDAModel.topicDistributions(documents);
	}
	
	/** 
	 * ������� ��������� �������� ������������� ���������������� ����������.
	 *
	 * @return JavaPairRDD<java.lang.Long,Vector> - ������� ������������� �� ������� ���������������� ����������.
	 */
	public JavaPairRDD<java.lang.Long,Vector> topicDistributions()
	{
		return TopicDistributions;
	}
	
	/** 
	 * ������� ���������  LDAModel (�������������).
	 * 
	 * @param JavaPairRDD<java.lang.Long,Vector> documents - ��������� ��� ��������� LDAModel.
	 * @param int k - ���������� �������
	 * @param int maxIterations - ������������ ���������� ��������
	 * 
	 * @return LDAModel LDAModel - ���������� LDAModel.
	 */
	public LDAModel getLDAModel(JavaPairRDD<java.lang.Long,Vector> documents, int k, int maxIterations)
	{
		org.apache.spark.mllib.clustering.LDA lda = new org.apache.spark.mllib.clustering.LDA();
		
		//���������� ��������� �������������
		lda.setK(k);
		lda.setMaxIterations(maxIterations);
		
		//��������������
		LDAModel ldamodel = lda.run(documents);
		
		//�������� �������������� ������
		DLDAModel = (DistributedLDAModel)ldamodel;
		
		//�������� ��������� ������
		LLDAModel = DLDAModel.toLocal();
		
		//�������� ������� ������������� �� ������� ���������������� ����������
		TopicDistributions = LLDAModel.topicDistributions(documents);
		
		return ldamodel;
	}
	
	/** 
	 * ���������� LDAModel.
	 * 
	 * @param SparkContext sc - ��������.
	 * @param java.lang.String path - ���� � ����� ����������.
	 * 
	 * @throws IOException 
	 */
	public void save(JavaSparkContext sc, java.lang.String path) throws IOException
	{
		//���������� �������������� ������
		DLDAModel.save(JavaSparkContext.toSparkContext(sc), path + "/DistributedLDAModel");
		
		//���������� countvector
		CVM.save(path + "/CountVectorizerModel");
		
		//���������� �������� ������������� ���������������� ����������
		TopicDistributions.saveAsTextFile(path + "/TopicsDistributions");
		
		return;
	}
	
	/** 
	 * �������� LDAModel.
	 * 
	 * @param SparkContext sc - ��������.
	 * @param java.lang.String path - ���� � ����� ��������.
	 */
	public void load(JavaSparkContext sc, java.lang.String path)
	{
		//�������� �������������� ������
		DLDAModel = DistributedLDAModel.load(JavaSparkContext.toSparkContext(sc), path + "/DistributedLDAModel");
		
		//�������� countvector
		CVM = CountVectorizerModel.load(path + "/CountVectorizerModel");
		
		//��������� ��������� ������
		LLDAModel = DLDAModel.toLocal();
		
		//�������� �������� ������������� ������������������ ����������
		JavaRDD <String> loadTopicDistributions = sc.textFile(path + "/TopicsDistributions");
		
		//�������������� ����������� ���������� � ���� JavaPairRDD<Long, Vector>
		TopicDistributions = loadTopicDistributions.mapToPair(new PairFunction<String, Long, Vector>()
				{
					@Override
					public Tuple2<Long, Vector> call(String line) throws Exception {
						//��������� id ��������� �� ������
						Long id = Long.valueOf(line.substring(1, line.indexOf(','))); 
						
						//��������� ��������� ������� ������������� � ������ �������
						String [] valuesString = line.substring(line.indexOf('[') + 1, line.indexOf(']')).split(",");
						
						//���������� ��������� � ���� double 
						double [] values = new double[valuesString.length];
						for(int i = 0; i < valuesString.length; i++)
							values[i] = Double.parseDouble(valuesString[i]);
						
						return new Tuple2<Long, Vector>(id, Vectors.dense(values));
					}
			
				});
	}
	
	/** 
	 * ������� ��������� �������������� ��������.
	 * 
	 * @param SparkContext sc - ��������.
	 * @param JavaRDD<String> documents- ��������� ��� ��������� �������� � ������� {id.txt\tText}.
	 * @param boolean createNewVocabulary - true-�������� ������ ������� ���� � ������� �����, false-������� ��������� �� ������ �������� �������.
	 * 
	 * @return JavaPairRDD<java.lang.Long,Vector> result = ���������� �������������� �������.
	 */
	public JavaPairRDD<java.lang.Long,Vector> getTermCountVectors(JavaSparkContext sc, JavaRDD<String> documents, boolean createNewVocabulary)
	{
		//sqlcontext ��� �������� dataframe
		SQLContext sqlContext = new SQLContext(sc);

		//���������� ������ ����� � ������ row
		JavaRDD<Row> rowRDD = documents.map(new Function<String, Row>() {
			@Override
            public Row call(String record) throws Exception {
				//��������� id ���������
                int idxFirstSpace = record.indexOf("\t");
                String id = record.substring(0, idxFirstSpace).replace(".txt", "");
                
                //��������� ������ ���� ���������
                String[] words = record.substring(idxFirstSpace+1).split(",");
                
                return RowFactory.create(Long.parseLong(id), words);
            }
        }
	    );
        
		//�������� dataframe �� ������ ������ row
        DataFrame df = sqlContext.createDataFrame(rowRDD, getSchema());
        
        //���� ���������� �������� ������ ������� ����
        if(createNewVocabulary)
        {CVM = new CountVectorizer()
          .setInputCol("id")//������� id
          .setInputCol("words")//������� words
          .setOutputCol("features")//�������� ������� - ���������� ��������� �����
          .setVocabSize(1000000000)//������������ ������ �������
          .setMinDF(2)//����������� df ��� �����
          
          .fit(df);}
        
        //������������ countvectors
		return CVM.transform(df)
	            .select("id", "features").toJavaRDD()
	            .mapToPair(new PairFunction<Row, Long, Vector>() {
	             
				public Tuple2<Long, Vector> call(Row row) throws Exception {
					
	                  return new Tuple2<Long, Vector>((Long)row.getAs(0), (Vector) row.getAs(1));
	              }
	            }).cache();
	}
	
	/**
	 * ������� ��������� ����� dataframe
	 * 
	 * @return StructType - ����� dataframe
	 */
	public StructType getSchema() {
		//���� �����
	    List<StructField> fields = new ArrayList<>();
	    
	    //����������� ���� id ���� Long
	    fields.add(DataTypes.createStructField("id", DataTypes.LongType, false));
	    
	    //����������� ���� words ���� String
	    fields.add(new StructField("words", new ArrayType(DataTypes.StringType, true), false, Metadata.empty()));
	    
	    return DataTypes.createStructType(fields);
	}
}
