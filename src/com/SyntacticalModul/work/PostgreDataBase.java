package com.SyntacticalModul.work;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.mllib.linalg.Vector;
import org.postgresql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;


/**
 * ����� ��� ������ � ����� ������ PostgreSQL
 */
public class PostgreDataBase {

	private Connection Connection;//����������� � ���� ������
	
	/**
	 * ����������� �� ���������. ������� ����������� � ����������� ���� ������.
	 *  
	 * @throws SQLException
	 */
	public PostgreDataBase () throws SQLException
	{
		//��������� �����������
		String url = "jdbc:postgresql://127.0.0.1:5432/postgres";
        String user = "postgres";
        String password = "utn22244";
        
        //������������� �������� PostgreSQL
        Driver driver = new Driver();
        
        //����������� � ���������� 
        Connection = DriverManager.getConnection(url,user,password);
	}
	
	/**
	 * ����������� � �����������.
	 * 
	 * @param String host - ���� ���� ������
	 * @param String port - ���� ���� ������
	 * @param String dbName - ��� ���� ������
	 * @param String user - ������������
	 * @param String password - � ������ ��� �����������
	 * 
	 * @throws SQLException
	 */
	public PostgreDataBase (String host,String port, String dbName, String user, String password) throws SQLException
	{
		//�������� �����������
		String url = "jdbc:postgresql://" + host + ":" + port + "/" + dbName;
        
		//������������� ��������
        Driver driver = new Driver();
        
        //����������� � ���� ������
        Connection = DriverManager.getConnection(url, user, password);
	}
	
	/**
	 * ������� ������� �������� � ������� ���� ������.
	 * ������� ������� ���� ����� �� ����������.
	 * �������� ������� ���� ����� ���������� ����� ����������� ��������.
	 * 
	 * @param String tableName - ��� �������, ���� ����������� �������
	 * @param JavaPairRDD <java.lang.Long, Vector> vectors - ����������� �������
	 * 
	 * @throws SQLException
	 */
	public void insertIntoTableVectors (String tableName, JavaPairRDD <java.lang.Long, Vector> vectors) throws SQLException
	{
		//sql ������
		String sqlRequest = "CREATE TABLE IF NOT EXISTS " + tableName + "(id_document integer PRIMARY KEY, val_vector double precision[]);DELETE FROM " + tableName + ";";
		
		//������� ��������
		Map <java.lang.Long, Vector> mapOfVectors = vectors.collectAsMap();
		
		//����� �������
		for (Map.Entry<java.lang.Long,Vector> entry : mapOfVectors.entrySet()) {
			
			//��������� ������� �� �������
			Vector value = entry.getValue();
			//��������� ������� � ������
			String vectorStr = "'{" + value.apply(0);
			for(int i = 1; i < value.size(); ++i)
			{
				vectorStr += "," + value.apply(i);
			}
			vectorStr += "}'";
			sqlRequest += "INSERT INTO "+ tableName + " VALUES ( " + entry.getKey() + "," + vectorStr + ");";
		}
		
		//���������� �������
		Statement statement = Connection.createStatement();
		statement.executeUpdate(sqlRequest);
		
	}
	
	/**
	 * ������� ������ �������� ������� PostgreSQL
	 * 
	 * @param String functionName - ��� �������
	 * @param Vector valuesOfVector - ������� ������ ��� �������
	 * @param int typeTable - ��� ������� ��� ������ (1,2 ��� 3)
	 * @param double delta - �������� ��� �������
	 * 
	 * @return ResultSet - ������������ �������� �������� �������
	 * 
	 * @throws SQLException
	 */
	public ResultSet callFunction (String functionName, Vector valuesOfVector, int typeTable, double delta) throws SQLException
	{
		//���������� ������ ������
		String valuesOfVectorStr = "'{" + valuesOfVector.apply(0);
		String indxsStr = "'{" + 1;
		
		for(int i = 1; i < valuesOfVector.size(); ++i)
		{
			valuesOfVectorStr += "," + valuesOfVector.apply(i);
			indxsStr += "," + (i+1);
		}
		valuesOfVectorStr += "}'";
		indxsStr += "}'";
		
		String sqlRequest = "SELECT DISTINCT " + functionName + "(" + valuesOfVectorStr + "," + indxsStr + "," +  delta + "," +  typeTable + ");";
		
		//���������� �������
		Statement statement = Connection.createStatement();
		return statement.executeQuery(sqlRequest);
	}
}
